package ge.openfinance.openproductsapi.impl.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;

@Configuration

public class SwaggerConfig {

	@Value("${SWAGGER_API_SERVER_URL:#{null}}")
	private String serverUrl;

	@Bean
	public OpenAPI customOpenAPI() {

		OpenAPI openapi = new OpenAPI().info(getAPIInfo());

		if (serverUrl != null) {
			Server server = new Server();
			server.setUrl(serverUrl);
			server.setDescription("Defined manually");

			openapi = openapi.servers(List.of(server));

		}

		return openapi;

	}

	private Info getAPIInfo() {
		return new Info()

				.title("Georgian OpenFinance OpenAPI")

				.version("0.1")

				.description("Description")

				.termsOfService("http://swagger.io/terms/")

				.license(new License().name("Apache 2.0").url("http://springdoc.org"));
	}
}
