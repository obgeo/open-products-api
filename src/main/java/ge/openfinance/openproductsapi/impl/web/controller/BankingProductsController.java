package ge.openfinance.openproductsapi.impl.web.controller;

import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ge.openfinance.openproductsapi.impl.entity.BankingProductByIdResponse;
import ge.openfinance.openproductsapi.impl.entity.BankingProductDetail;
import ge.openfinance.openproductsapi.impl.entity.BankingProductListResponse;
import ge.openfinance.openproductsapi.impl.service.BankingProductService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/banking/products")
@Slf4j
public class BankingProductsController {

	private final BankingProductService bankingProductService;

	@Autowired
	public BankingProductsController(BankingProductService bankingProductService) {
		this.bankingProductService = bankingProductService;
	}

	@GetMapping("")
	public BankingProductListResponse getProducts() {
		log.info("process=get-products");

		return BankingProductListResponse.builder().products(bankingProductService.getAllBankingProducts()).build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<BankingProductByIdResponse> getProductById(@PathVariable String id) {
		log.info("process=get-product-by-id, id={}", id);
		Optional<BankingProductDetail> user = bankingProductService.getBankingProductById(id);

		Function<BankingProductDetail, ResponseEntity<BankingProductByIdResponse>> mapper = u -> ResponseEntity
				.ok(BankingProductByIdResponse.builder().product(u).build());

		return user.map(mapper).orElse(ResponseEntity.notFound().build());
	}

}
