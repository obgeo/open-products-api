package ge.openfinance.openproductsapi.impl.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ge.openfinance.openproductsapi.impl.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
