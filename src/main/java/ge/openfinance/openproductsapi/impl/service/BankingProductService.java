package ge.openfinance.openproductsapi.impl.service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ge.openfinance.openproductsapi.impl.entity.BankingProduct;
import ge.openfinance.openproductsapi.impl.entity.BankingProductAdditionalInformation;
import ge.openfinance.openproductsapi.impl.entity.BankingProductCardArt;
import ge.openfinance.openproductsapi.impl.entity.BankingProductCategory;
import ge.openfinance.openproductsapi.impl.entity.BankingProductDetail;
import ge.openfinance.openproductsapi.impl.repo.BankingProductDetailRepository;

@Service
@Transactional
public class BankingProductService {
	private final BankingProductDetailRepository bankingProductDetailRepository;

	@Autowired
	public BankingProductService(BankingProductDetailRepository bankingProductDetailRepository) {
		this.bankingProductDetailRepository = bankingProductDetailRepository;
	}

	public Optional<BankingProductDetail> getBankingProductById(String id) {
		return bankingProductDetailRepository.findById(id);
	}

	public List<BankingProduct> getAllBankingProducts() {

		return bankingProductDetailRepository.findAllProjectedBy().map(product -> shrink(product))
				.collect(Collectors.toList());

	}

	protected BankingProduct shrink(final BankingProductDetail original) {
		return new BankingProduct() {

			@Override
			public boolean isTailored() {
				return original.isTailored();
			}

			@Override
			public String getProductId() {
				return original.getProductId();
			}

			@Override
			public BankingProductCategory getProductCategory() {
				return original.getProductCategory();
			}

			@Override
			public String getName() {
				return original.getName();
			}

			@Override
			public ZonedDateTime getLastUpdated() {
				return original.getLastUpdated();
			}

			@Override
			public ZonedDateTime getEffectiveTo() {
				return original.getEffectiveTo();
			}

			@Override
			public ZonedDateTime getEffectiveFrom() {
				return original.getEffectiveFrom();
			}

			@Override
			public String getDescription() {
				return original.getDescription();
			}

			@Override
			public BankingProductCardArt getCardArt() {
				return original.getCardArt();
			}

			@Override
			public String getBrandName() {
				return original.getBrandName();
			}

			@Override
			public String getBrand() {
				return original.getBrand();
			}

			@Override
			public String getApplicationUri() {

				return original.getApplicationUri();
			}

			@Override
			public BankingProductAdditionalInformation getAdditionalInformation() {

				return original.getAdditionalInformation();
			}
		};

	}
}
