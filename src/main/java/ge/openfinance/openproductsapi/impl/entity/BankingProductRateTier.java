package ge.openfinance.openproductsapi.impl.entity;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@IdClass(BankingProductL2ChildId.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductRateTier {
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "product_id")
	private String productId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id")
	private int elementId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id_2")
	private int elementId2;

	@JsonProperty(required = true)
	private String name;

	@Enumerated(EnumType.STRING)
	private UnitOfMeasureType unitOfMeasure;

	private double minimumValue;

	@JsonInclude(value = Include.NON_NULL)
	private Double maximumValue;

	@JsonInclude(value = Include.NON_NULL)
	private String rateApplicationMethod;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfo;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfoUri;

	@JsonInclude(value = Include.NON_NULL)
	private String applCondAdditionalInfo;

	@JsonInclude(value = Include.NON_NULL)
	private String applCondAdditionalInfoUri;
}
