package ge.openfinance.openproductsapi.impl.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductByIdResponse {
	@JsonPropertyDescription("Banking product with details")
	private BankingProductDetail product;

	@JsonProperty("_links")
	@JsonPropertyDescription("Hyperlinks according to HAL")
	private Links links;
}
