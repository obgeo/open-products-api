package ge.openfinance.openproductsapi.impl.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@IdClass(BankingProductL1ChildId.class)
@Table(name = "banking_product_fees")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductFee {
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "product_id")
	private String productId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id")
	private int elementId;

	@JsonProperty(required = true)
	private String name;

	@JsonProperty(required = true)
	@Enumerated(EnumType.STRING)
	private BankingProductFeeType feeType;

	@JsonInclude(value = Include.NON_NULL)
	private Double amount;

	@JsonInclude(value = Include.NON_NULL)
	private Double balanceRate;

	@JsonInclude(value = Include.NON_NULL)
	private Double transactionRate;

	@JsonInclude(value = Include.NON_NULL)
	private Double accruedRate;

	@JsonInclude(value = Include.NON_NULL)
	private String accrualFrequency;

	@JsonInclude(value = Include.NON_NULL)
	private String currency;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalValue;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfo;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfoUri;

	@OneToMany
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	@JoinColumn(name = "element_id", referencedColumnName = "element_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductDiscount> discounts;
}
