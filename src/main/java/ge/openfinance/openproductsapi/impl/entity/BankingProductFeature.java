package ge.openfinance.openproductsapi.impl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@IdClass(BankingProductL1ChildId.class)
@Table(name = "banking_product_features")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductFeature {
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "product_id")
	private String productId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	private int elementId;

	@JsonProperty(required = true)
	@Enumerated(EnumType.STRING)
	private BankingProductFeatureType featureType;

	private String additionalValue;

	private String additionalInfo;

	private String additionalInfoUri;
}
