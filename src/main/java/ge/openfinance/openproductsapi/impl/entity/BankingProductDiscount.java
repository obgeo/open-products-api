package ge.openfinance.openproductsapi.impl.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "banking_product_discounts")
@IdClass(BankingProductL2ChildId.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductDiscount {
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "product_id")
	private String productId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id")
	private int elementId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id_2")
	private int elementId2;

	@JsonProperty(required = true)
	private String description;

	@Enumerated(EnumType.STRING)
	private BankingProductDiscountType discountType;

	@JsonInclude(value = Include.NON_NULL)
	private Double amount;

	@JsonInclude(value = Include.NON_NULL)
	private Double balanceRate;

	@JsonInclude(value = Include.NON_NULL)
	private Double transactionRate;
	
	@JsonInclude(value = Include.NON_NULL)
	private Double accruedRate;
	
	@JsonInclude(value = Include.NON_NULL)
	private Double feeRate;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfo;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfoUri;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalValue;
	
	@OneToMany
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	@JoinColumn(name = "element_id", referencedColumnName = "element_id")
	@JoinColumn(name = "element_id_2", referencedColumnName = "element_id_2")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductDiscountEligibility> eligibility;	
}
