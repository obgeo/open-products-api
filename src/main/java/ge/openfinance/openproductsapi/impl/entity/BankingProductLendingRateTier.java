package ge.openfinance.openproductsapi.impl.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "banking_product_lending_rate_tiers")
public class BankingProductLendingRateTier extends BankingProductRateTier {

}
