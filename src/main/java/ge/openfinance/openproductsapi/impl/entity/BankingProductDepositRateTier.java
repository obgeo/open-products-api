package ge.openfinance.openproductsapi.impl.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "banking_product_deposit_rate_tiers")
public class BankingProductDepositRateTier extends BankingProductRateTier {

}
