package ge.openfinance.openproductsapi.impl.entity;

import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "banking_products")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonPropertyOrder({ "productId", "effectiveFrom", "effectiveTo", "lastUpdated", "productCategory", "name",
		"description", "brand", "brandName", "applicationUri", "tailored", "additionalInformation", "cardArt",
		"features", "constraints", "eligibility", "fees", "depositRates", "lendingRates" })
public class BankingProductDetail implements BankingProduct {
	@Id
	@EqualsAndHashCode.Include
	@JsonProperty(required = true)
	@Column(name = "product_id")
	private String productId;

	@JsonProperty(required = true)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
	private ZonedDateTime effectiveFrom;

	@JsonInclude(value = Include.NON_NULL)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
	private ZonedDateTime effectiveTo;

	@JsonProperty(required = true)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
	private ZonedDateTime lastUpdated;

	@JsonProperty(required = true)
	@Enumerated(EnumType.STRING)
	private BankingProductCategory productCategory;

	@JsonProperty(required = true)
	private String name;

	@JsonProperty(required = true)
	private String description;

	@JsonProperty(required = true)
	private String brand;

	@JsonInclude(value = Include.NON_NULL)
	private String brandName;

	@JsonInclude(value = Include.NON_NULL)
	private String applicationUri;

	@JsonProperty(value = "isTailored", required = true)
	private boolean tailored;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_NULL)
	private BankingProductAdditionalInformation additionalInformation;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_NULL)
	private BankingProductCardArt cardArt;

	@OneToMany
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductFeature> features;

	@OneToMany
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductConstraint> constraints;

	@OneToMany
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductEligibility> eligibility;

	@OneToMany
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductDepositRate> depositRates;

	@OneToMany
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductLendingRate> lendingRates;

	@OneToMany
	@JoinColumn(name = "product_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductFee> fees;
}
