package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductDepositRateType {
	BONUS, BUNDLE_BONUS, FIXED, FLOATING, INTRODUCTORY, MARKET_LINKED, VARIABLE
}
