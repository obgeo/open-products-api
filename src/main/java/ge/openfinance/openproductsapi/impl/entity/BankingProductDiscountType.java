package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductDiscountType {
	BALANCE, DEPOSITS, ELIGIBILITY_ONLY, FEE_CAP, PAYMENTS
}
