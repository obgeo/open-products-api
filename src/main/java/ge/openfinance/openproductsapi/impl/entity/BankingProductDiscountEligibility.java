package ge.openfinance.openproductsapi.impl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "banking_product_discount_eligibilities")
@IdClass(BankingProductL3ChildId.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductDiscountEligibility {
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "product_id")
	private String productId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id")
	private int elementId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id_2")
	private int elementId2;
	
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id_3")
	private int elementId3;
	
	@Enumerated(EnumType.STRING)
	private BankingProductDiscountEligibilityType discountEligibilityType;
	
	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfo;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfoUri;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalValue;

}
