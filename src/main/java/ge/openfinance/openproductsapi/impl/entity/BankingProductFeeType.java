package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductFeeType {
	DEPOSIT, EVENT, EXIT, PAYMENT, PERIODIC, PURCHASE, TRANSACTION, UPFRONT, VARIABLE, WITHDRAWAL
}
