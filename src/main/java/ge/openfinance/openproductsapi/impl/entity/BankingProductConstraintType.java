package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductConstraintType {
	MAX_BALANCE, MAX_LIMIT, MIN_BALANCE, MIN_LIMIT, OPENING_BALANCE
}
