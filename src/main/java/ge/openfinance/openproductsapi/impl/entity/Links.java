package ge.openfinance.openproductsapi.impl.entity;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Links {

	@JsonPropertyDescription("Hyperlink pointing to this entity")
	private URIString self;

	@JsonPropertyDescription("Hyperlink pointing to the next entity. Empty or null value means that the current entity is the last one")
	private URIString next;
}
