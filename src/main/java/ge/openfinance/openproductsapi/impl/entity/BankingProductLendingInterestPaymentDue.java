package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductLendingInterestPaymentDue {
	IN_ADVANCE, IN_ARREARS
}
