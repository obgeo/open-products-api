package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductLendingLoanPurpose {
	INVESTMENT, OWNER_OCCUPIED
}
