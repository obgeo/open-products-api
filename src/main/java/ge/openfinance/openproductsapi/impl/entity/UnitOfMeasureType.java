package ge.openfinance.openproductsapi.impl.entity;

public enum UnitOfMeasureType {
	DAY, AMOUNT, MONTH, PERCENT, PER_TIER, WHOLE_BALANCE
}
