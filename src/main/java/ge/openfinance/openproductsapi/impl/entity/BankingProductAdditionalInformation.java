package ge.openfinance.openproductsapi.impl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "banking_product_infos")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductAdditionalInformation {
	
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name="product_id")
	private String productId;
	
	private String eligibilityUri;
	
	private String feesAndPricingUri;
	
	private String overviewUri;
	
	private String termsUri;
}
