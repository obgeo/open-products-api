package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductLendingRepaymentType {
	INTEREST_ONLY, PRINCIPAL_AND_INTEREST
}
