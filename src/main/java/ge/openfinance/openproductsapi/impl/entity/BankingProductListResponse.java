package ge.openfinance.openproductsapi.impl.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductListResponse {

	@JsonPropertyDescription("Banking product list")
	private List<BankingProduct> products;

	@JsonProperty("_links")
	@JsonPropertyDescription("Hyperlinks according to HAL")
	private Links links;
}
