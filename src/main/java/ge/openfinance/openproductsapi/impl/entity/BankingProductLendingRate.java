package ge.openfinance.openproductsapi.impl.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@IdClass(BankingProductL1ChildId.class)
@Table(name = "banking_product_lending_rates")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankingProductLendingRate {
	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "product_id")
	private String productId;

	@Id
	@EqualsAndHashCode.Include
	@JsonIgnore
	@Column(name = "element_id")
	private int elementId;

	@JsonProperty(required = true)
	@Enumerated(EnumType.STRING)
	private BankingProductLendingRateType lendingRateType;

	@JsonProperty(required = true)
	private double rate;

	private double comparisonRate;

	@JsonInclude(value = Include.NON_NULL)
	private String calculationFrequency;
	
	@JsonInclude(value = Include.NON_NULL)
	@Enumerated(EnumType.STRING)
	private BankingProductLendingInterestPaymentDue interestPaymentDue;

	@JsonInclude(value = Include.NON_NULL)
	@Enumerated(EnumType.STRING)
	private BankingProductLendingRepaymentType repaymentType;

	@JsonInclude(value = Include.NON_NULL)
	@Enumerated(EnumType.STRING)
	private BankingProductLendingLoanPurpose loanPurpose;

	@OneToMany
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	@JoinColumn(name = "element_id", referencedColumnName = "element_id")
	@JsonInclude(value = Include.NON_EMPTY)
	private List<BankingProductLendingRateTier> tiers;

	@JsonInclude(value = Include.NON_NULL)
	private String applicationFrequency;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalValue;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfo;

	@JsonInclude(value = Include.NON_NULL)
	private String additionalInfoUri;

}
