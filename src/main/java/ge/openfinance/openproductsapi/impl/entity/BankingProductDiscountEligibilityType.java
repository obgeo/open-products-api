package ge.openfinance.openproductsapi.impl.entity;

public enum BankingProductDiscountEligibilityType {
	BUSINESS, EMPLOYMENT_STATUS, INTRODUCTORY, MAX_AGE, MIN_AGE, MIN_INCOME, MIN_TURNOVER, NATURAL_PERSON, OTHER, PENSION_RECIPIENT, RESIDENCY_STATUS, STAFF, STUDENT
}
