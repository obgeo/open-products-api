package ge.openfinance.openproductsapi.impl.entity;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DateTimeString {
	@JsonValue
	private ZonedDateTime value;

	public ZonedDateTime getValue() {
		return value;
	}

	public void setValue(ZonedDateTime value) {
		this.value = value;
	}
	
}
