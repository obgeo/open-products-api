package ge.openfinance.openproductsapi.impl.entity;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "productId", "effectiveFrom", "effectiveTo", "lastUpdated", "productCategory", "name",
		"description", "brand", "brandName", "applicationUri", "tailored", "additionalInformation", "cardArt" })
public interface BankingProduct {
	
	@JsonProperty(required = true)
	String getProductId();
	
	@JsonProperty(required = true)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
	ZonedDateTime getEffectiveFrom();
	
	@JsonInclude(value = Include.NON_NULL)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
	ZonedDateTime getEffectiveTo();
	
	@JsonProperty(required = true)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
	ZonedDateTime getLastUpdated();
	
	@JsonProperty(required = true)
	BankingProductCategory getProductCategory();
	
	@JsonProperty(required = true)
	String getName();
	
	@JsonProperty(required = true)
	String getDescription();
	
	@JsonProperty(required = true)
	String getBrand();
	
	String getBrandName();
	
	
	String getApplicationUri();
	
	@JsonProperty(value = "isTailored", required = true)
	boolean isTailored();
	
	@JsonInclude(value = Include.NON_EMPTY)
	BankingProductAdditionalInformation getAdditionalInformation();
	
	@JsonInclude(value = Include.NON_EMPTY)
	BankingProductCardArt getCardArt();
}
