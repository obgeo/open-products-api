INSERT INTO public.banking_products (dtype,product_id,application_uri,brand,brand_name,description,effective_from,effective_to,last_updated,"name",product_category,tailored) VALUES
	 (0,'b4f9f303-2634-4b0d-94c5-84b6a8395920','https://bank.ob/personal-loan/start','OB','OBTravel','იპოთეკური სესხი ფიზიკური პირებისათვის, სხვადასხვა ვარიანტებით','2020-01-31 13:00:00',NULL,'2021-12-15 00:59:00','იპოთეკური სესხი ფიზიკური პირებისათვის','PERS_LOANS',false);
	 
INSERT INTO public.banking_product_constraints (product_id,element_id,additional_info,additional_info_uri,additional_value,constraint_type) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,NULL,NULL,'5000.00','MIN_LIMIT'),
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',1,NULL,NULL,'50000.00','MAX_LIMIT');
	 
INSERT INTO public.banking_product_eligibilities (product_id,element_id,additional_info,additional_info_uri,additional_value,eligibility_type) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,'პროდუქტი განკუთვნილია მხოლოდ ფიზიკური პირებისათვის',NULL,NULL,'NATURAL_PERSON'),
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',1,'უნდა იყოთ 18 წლის ან მეტის',NULL,'18','MIN_AGE'),
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',2,'ყოველთვიური შემოსავალი უნდა გქონდეთ არანაკლებ 15000 ლარისა',NULL,'15000.00','MIN_INCOME'),
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',3,'უნდა იყოთ საქართველოს რეზიდენტი','https://bank.test/faqs/#visa?CID','GE','RESIDENCY_STATUS');

INSERT INTO public.banking_product_features (product_id,element_id,additional_info,additional_info_uri,additional_value,feature_type) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,'ულიმიტო ტრანზაქციები ხელმისაწვდომია ინტერნეტ ბანკინგის, სატელეფონო ბანკის, ბანკომატების გამოყენებით',NULL,NULL,'UNLIMITED_TXNS'),
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',1,'პაკეტში შედის ინტერნეტ და მობილბანკი','https://bank.test/personal/CID',NULL,'DIGITAL_BANKING'),
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',2,'თუ თქვენ გაქვთ ან სესხით სარგებლობთ, თქვენ შეგიძლიათ მიიღოთ წვდომა თქვენს მიერ დაფარულ დამატებით ფულად რესურსზე. თქვენ შეგიძლიათ აიღოთ თანხა, რომელიც გადაიხადეთ თქვენი მინიმალური ყოველთვიური დაფარვის ზემოთ','https://bank.test/faqs/loan_redraw_howto',NULL,'REDRAW');
	 
INSERT INTO public.banking_product_infos (product_id,eligibility_uri,fees_and_pricing_uri,overview_uri,terms_uri) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920','https://test.ob/variable-rate/eligibility','https://test.ob/variable-rate/eligibility','https://test.ob/variable-rate','http://test.ob/fees-terms-conditions');
	 
INSERT INTO public.banking_product_lending_rates (product_id,element_id,additional_info,additional_info_uri,additional_value,application_frequency,calculation_frequency,comparison_rate,interest_payment_due,lending_rate_type,loan_purpose,rate,repayment_type) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,'10-წლიანი იპოთეკური სესხი, ძირის და პროცენტის ყოველთვიური გადახდით, ფიქსირებული საპროცენტო განაკვეთით,  <=70% LVR',NULL,'P10Y','P1M','P1D',0.1138,'IN_ARREARS','FIXED','INVESTMENT',0.105,'PRINCIPAL_AND_INTEREST');
	 
INSERT INTO public.banking_product_lending_rate_tiers (product_id,element_id,element_id_2,additional_info,additional_info_uri,appl_cond_additional_info,appl_cond_additional_info_uri,maximum_value,minimum_value,"name",rate_application_method,unit_of_measure) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,0,NULL,NULL,NULL,NULL,70.0,0.0,'LVR 70%-მდე','WHOLE_BALANCE','PERCENT');

INSERT INTO public.banking_product_fees (product_id,element_id,accrual_frequency,accrued_rate,additional_info,additional_info_uri,additional_value,amount,balance_rate,currency,fee_type,"name",transaction_rate) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,NULL,NULL,NULL,NULL,NULL,150.0,NULL,'GEL','UPFRONT','სესხის დამტკიცების საკომისიო',NULL),
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',1,NULL,NULL,NULL,NULL,'P1M',10.0,NULL,'GEL','PERIODIC','სესხის მომსახურების გადასახადი',NULL);
	 
INSERT INTO public.banking_product_discounts (product_id,element_id,element_id_2,accrued_rate,additional_info,additional_info_uri,additional_value,amount,balance_rate,description,discount_type,fee_rate,transaction_rate) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,0,NULL,NULL,NULL,NULL,NULL,NULL,'25% ფასდაკლება სპეციალური კატეგორიისათვის','ELIGIBILITY_ONLY',0.25,NULL);
	 
INSERT INTO public.banking_product_discount_eligibilities (product_id,element_id,element_id_2,element_id_3,additional_info,additional_info_uri,additional_value,discount_eligibility_type) VALUES
	 ('b4f9f303-2634-4b0d-94c5-84b6a8395920',0,0,0,'ღია ბანკინგის პროექტის თანამშრომლები',NULL,NULL,'STAFF');
	