create sequence user_id_seq start with 10 increment by 50;

CREATE TABLE public.users (
	id int8 NOT NULL,
	created_at timestamp NULL,
	email varchar(255) NOT NULL,
	"name" varchar(255) NOT NULL,
	updated_at timestamp NULL,
	CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email),
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

-- public.banking_product_cardarts definition

-- Drop table

-- DROP TABLE public.banking_product_cardarts;

CREATE TABLE public.banking_product_cardarts (
	product_id varchar(255) NOT NULL,
	image_uri varchar(255) NULL,
	title varchar(255) NULL,
	CONSTRAINT banking_product_cardarts_pkey PRIMARY KEY (product_id)
);


-- public.banking_product_infos definition

-- Drop table

-- DROP TABLE public.banking_product_infos;

CREATE TABLE public.banking_product_infos (
	product_id varchar(255) NOT NULL,
	eligibility_uri varchar(255) NULL,
	fees_and_pricing_uri varchar(255) NULL,
	overview_uri varchar(255) NULL,
	terms_uri varchar(255) NULL,
	CONSTRAINT banking_product_infos_pkey PRIMARY KEY (product_id)
);


-- public.banking_products definition

-- Drop table

-- DROP TABLE public.banking_products;

CREATE TABLE public.banking_products (
	dtype int4 NOT NULL,
	product_id varchar(255) NOT NULL,
	application_uri varchar(255) NULL,
	brand varchar(255) NULL,
	brand_name varchar(255) NULL,
	description varchar(255) NULL,
	effective_from timestamp with time zone NULL,
	effective_to timestamp with time zone NULL,
	last_updated timestamp with time zone NULL,
	"name" varchar(255) NULL,
	product_category varchar(255) NULL,
	tailored bool NOT NULL,
	CONSTRAINT banking_products_pkey PRIMARY KEY (product_id)
);


-- public.banking_product_constraints definition

-- Drop table

-- DROP TABLE public.banking_product_constraints;

CREATE TABLE public.banking_product_constraints (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	constraint_type varchar(255) NULL,
	CONSTRAINT banking_product_constraints_pkey PRIMARY KEY (product_id, element_id),
	CONSTRAINT fkg705otvkmd99wdgchpqmp43n FOREIGN KEY (product_id) REFERENCES public.banking_products(product_id)
);


-- public.banking_product_deposit_rates definition

-- Drop table

-- DROP TABLE public.banking_product_deposit_rates;

CREATE TABLE public.banking_product_deposit_rates (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	application_frequency varchar(255) NULL,
	calculation_frequency varchar(255) NULL,
	comparison_rate float8 NOT NULL,
	deposit_rate_type varchar(255) NULL,
	rate float8 NOT NULL,
	CONSTRAINT banking_product_deposit_rates_pkey PRIMARY KEY (product_id, element_id),
	CONSTRAINT fko9gvl59ouc6nbgtbshmmvqmh9 FOREIGN KEY (product_id) REFERENCES public.banking_products(product_id)
);


-- public.banking_product_eligibilities definition

-- Drop table

-- DROP TABLE public.banking_product_eligibilities;

CREATE TABLE public.banking_product_eligibilities (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	eligibility_type varchar(255) NULL,
	CONSTRAINT banking_product_eligibilities_pkey PRIMARY KEY (product_id, element_id),
	CONSTRAINT fkqtx12wx8bhxt0p7a2cplesr09 FOREIGN KEY (product_id) REFERENCES public.banking_products(product_id)
);


-- public.banking_product_features definition

-- Drop table

-- DROP TABLE public.banking_product_features;

CREATE TABLE public.banking_product_features (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	feature_type varchar(255) NULL,
	CONSTRAINT banking_product_features_pkey PRIMARY KEY (product_id, element_id),
	CONSTRAINT fkljt0m3pabdvn3kct551vw0y2m FOREIGN KEY (product_id) REFERENCES public.banking_products(product_id)
);


-- public.banking_product_lending_rates definition

-- Drop table

-- DROP TABLE public.banking_product_lending_rates;

CREATE TABLE public.banking_product_lending_rates (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	application_frequency varchar(255) NULL,
	calculation_frequency varchar(255) NULL,
	comparison_rate float8 NOT NULL,
	interest_payment_due varchar(255) NULL,
	lending_rate_type varchar(255) NULL,
	loan_purpose varchar(255) NULL,
	rate float8 NOT NULL,
	repayment_type varchar(255) NULL,
	CONSTRAINT banking_product_lending_rates_pkey PRIMARY KEY (product_id, element_id),
	CONSTRAINT fkjn2h2quo4av26udym1bxng32 FOREIGN KEY (product_id) REFERENCES public.banking_products(product_id)
);


-- public.banking_product_deposit_rate_tiers definition

-- Drop table

-- DROP TABLE public.banking_product_deposit_rate_tiers;

CREATE TABLE public.banking_product_deposit_rate_tiers (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	element_id_2 int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	appl_cond_additional_info varchar(255) NULL,
	appl_cond_additional_info_uri varchar(255) NULL,
	maximum_value float8 NULL,
	minimum_value float8 NOT NULL,
	"name" varchar(255) NULL,
	rate_application_method varchar(255) NULL,
	unit_of_measure varchar(255) NULL,
	CONSTRAINT banking_product_deposit_rate_tiers_pkey PRIMARY KEY (product_id, element_id, element_id_2),
	CONSTRAINT fknumucr9a4705u14gxe0nlnyih FOREIGN KEY (product_id, element_id) REFERENCES public.banking_product_deposit_rates(product_id, element_id)
);


-- public.banking_product_lending_rate_tiers definition

-- Drop table

-- DROP TABLE public.banking_product_lending_rate_tiers;

CREATE TABLE public.banking_product_lending_rate_tiers (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	element_id_2 int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	appl_cond_additional_info varchar(255) NULL,
	appl_cond_additional_info_uri varchar(255) NULL,
	maximum_value float8 NULL,
	minimum_value float8 NOT NULL,
	"name" varchar(255) NULL,
	rate_application_method varchar(255) NULL,
	unit_of_measure varchar(255) NULL,
	CONSTRAINT banking_product_lending_rate_tiers_pkey PRIMARY KEY (product_id, element_id, element_id_2),
	CONSTRAINT fkacy41qr00pd5vam1tb7f224os FOREIGN KEY (product_id, element_id) REFERENCES public.banking_product_lending_rates(product_id, element_id)
);

-- public.banking_product_fees definition

-- Drop table

-- DROP TABLE public.banking_product_fees;

CREATE TABLE public.banking_product_fees (
	element_id int4 NOT NULL,
	product_id varchar(255) NOT NULL,
	accrual_frequency varchar(255) NULL,
	accrued_rate float8 NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	amount float8 NULL,
	balance_rate float8 NULL,
	currency varchar(255) NULL,
	fee_type varchar(255) NULL,
	"name" varchar(255) NULL,
	transaction_rate float8 NULL,
	CONSTRAINT banking_product_fees_pkey PRIMARY KEY (element_id, product_id),
	CONSTRAINT fkqesqmrqfjcx2v45v88jmqbr3s FOREIGN KEY (product_id) REFERENCES public.banking_products(product_id)
);

-- public.banking_product_discounts definition

-- Drop table

-- DROP TABLE public.banking_product_discounts;

CREATE TABLE public.banking_product_discounts (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	element_id_2 int4 NOT NULL,
	accrued_rate float8 NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	amount float8 NULL,
	balance_rate float8 NULL,
	description varchar(255) NULL,
	discount_type varchar(255) NULL,
	fee_rate float8 NULL,
	transaction_rate float8 NULL,
	CONSTRAINT banking_product_discounts_pkey PRIMARY KEY (product_id, element_id, element_id_2),
	CONSTRAINT fk6h0ofrnbk1w2ttd86vmrvdol0 FOREIGN KEY (product_id, element_id) REFERENCES public.banking_product_fees(product_id, element_id)
);


-- public.banking_product_discount_eligibilities definition

-- Drop table

-- DROP TABLE public.banking_product_discount_eligibilities;

CREATE TABLE public.banking_product_discount_eligibilities (
	product_id varchar(255) NOT NULL,
	element_id int4 NOT NULL,
	element_id_2 int4 NOT NULL,
	element_id_3 int4 NOT NULL,
	additional_info varchar(255) NULL,
	additional_info_uri varchar(255) NULL,
	additional_value varchar(255) NULL,
	discount_eligibility_type varchar(255) NULL,
	CONSTRAINT banking_product_discount_eligibilities_pkey PRIMARY KEY (product_id, element_id, element_id_2, element_id_3),
	CONSTRAINT fkfxjm4cvbp4iruhfvj8wv8pv94 FOREIGN KEY (product_id, element_id,element_id_2) REFERENCES public.banking_product_discounts(product_id, element_id,element_id_2)
);

