
INSERT INTO users (email, name, created_at) VALUES
('admin@gmail.com', 'Admin', CURRENT_TIMESTAMP()),
('siva@gmail.com', 'Siva', CURRENT_TIMESTAMP())
;

INSERT INTO banking_products (product_id, dtype, product_category, brand, brand_name, description, effective_from, effective_to, last_updated, name, tailored, application_uri) VALUES
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 0, 'PERS_LOANS', 'OB', 'OBTravel', 'სამომხმარებლოს სესხის მოკლე აღწერა', parsedatetime('2020-01-31 13:00:00.069', 'yyyy-MM-dd HH:mm:ss.SSS'), null, parsedatetime('2021-12-15 00:59:00.000', 'yyyy-MM-dd HH:mm:ss.SSS'), 'სამომხმარებლო სესხი', false, 'https://bank.ob/personal-loan/start')
;


INSERT INTO banking_product_infos (product_id, eligibility_uri,fees_and_pricing_uri,overview_uri, terms_uri) VALUES
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 'https://test.ob/variable-rate/eligibility', 'https://test.ob/variable-rate/rates', 'https://test.ob/variable-rate', 'http://test.ob/fees-terms-conditions')
;

INSERT INTO banking_product_constraints (product_id, element_id, constraint_type, additional_info, additional_info_uri, additional_value) VALUES
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 0, 'MIN_LIMIT', null, null, '5000.00'),
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 1, 'MAX_LIMIT', null, null, '50000.00')
;

INSERT INTO banking_product_eligibilities (product_id, element_id, eligibility_type, additional_info, additional_info_uri, additional_value) VALUES 
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 0, 'NATURAL_PERSON', null, null, null),
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 1, 'MIN_AGE', 'უნდა იყოთ 18 წლის ან მეტის', null, '18'),
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 2, 'MIN_INCOME', 'ყოველთვიური შემოსავალი უნდა გქონდეთ არანაკლებ 15000 ლარისა', null, '15000.00'),
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 3, 'RESIDENCY_STATUS', 'უნდა იყოთ საქართველოს რეზიდენტი', 'https://bank.test/faqs/#visa?CID', null)
;	

INSERT INTO banking_product_features (product_id, element_id, feature_type, additional_info, additional_info_uri, additional_value) VALUES
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 0, 'UNLIMITED_TXNS', 'ულიმიტო ტრანზაქციები ხელმისაწვდომია ინტერნეტ ბანკინგის, სატელეფონო ბანკის, ბანკომატების გამოყენებით', null, null),
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 1, 'DIGITAL_BANKING', 'პაკეტში შედის ინტერნეტ და მობილბანკი', 'https://bank.test/personal/CID', null),
('b4f9f303-2634-4b0d-94c5-84b6a8395920', 2, 'REDRAW', 'თუ თქვენ გაქვთ პერსონალური სესხი, საშუალებას გაძლევთ მიიღოთ წვდომა თქვენს მიერ დაფარულ დამატებით ფულზე. თქვენ შეგიძლიათ აიღოთ თანხა, რომელიც გადაიხადეთ თქვენი მინიმალური ყოველთვიური დაფარვის ზემოთ', 'https://bank.test//faqs/CID', null)
;	
