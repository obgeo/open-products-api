create sequence user_id_seq start with 10 increment by 50;

create table users (
    id bigint default user_id_seq.nextval,
    email varchar(255) not null,
    name varchar(255) not null,
    created_at timestamp,
    updated_at timestamp,
    primary key (id),
    UNIQUE KEY user_email_unique (email)
);

create table banking_products (
	product_id varchar(255) not null,
	dtype integer,
	application_uri varchar(255), 
	brand varchar(255), 
	brand_name varchar(255), 
	description varchar(255), 
	effective_from timestamp, 
	effective_to timestamp, 
	last_updated timestamp, 
	name varchar(255), 
	product_category varchar(255), 
	tailored boolean not null, 
	primary key (product_id)
);

create table banking_product_cardarts (
	product_id varchar(255) not null, 
	image_uri varchar(255), 
	title varchar(255), 
	primary key (product_id)
);

alter table banking_product_cardarts add constraint FK68osb8yhb3cvn833c30tgnjb foreign key (product_id) references banking_products;

create table banking_product_infos (
	product_id varchar(255) not null, 
	eligibility_uri varchar(255),
	fees_and_pricing_uri varchar(255),
	overview_uri varchar(255),
	terms_uri varchar(255),
	primary key (product_id)
);

alter table banking_product_infos add constraint FKa4egd75kjrb26uitsaw1qdi3n foreign key (product_id) references banking_products;

create table banking_product_constraints (
	product_id varchar(255) not null,
	element_id integer not null,
	constraint_type varchar(255), 	
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	additional_value varchar(255), 
	primary key (product_id, element_id)
);

alter table banking_product_constraints add constraint FKg705otvkmd99wdgchpqmp43n foreign key (product_id) references banking_products;

create table banking_product_eligibilities (
	product_id varchar(255) not null, 
	element_id integer not null, 
	eligibility_type varchar(255),	
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	additional_value varchar(255), 
	primary key (product_id, element_id)
);

alter table banking_product_eligibilities add constraint FKqtx12wx8bhxt0p7a2cplesr09 foreign key (product_id) references banking_products;

create table banking_product_features (
	product_id varchar(255) not null, 
	element_id integer not null, 
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	additional_value varchar(255), 
	feature_type varchar(255), 
	primary key (product_id, element_id)
);

alter table banking_product_features add constraint FKljt0m3pabdvn3kct551vw0y2m foreign key (product_id) references banking_products;

create table banking_product_deposit_rates (
	product_id varchar(255) not null,
	element_id integer not null, 
	deposit_rate_type varchar(255), 
	rate double not null, 
	comparison_rate double not null, 
	calculation_frequency varchar(255), 
	application_frequency varchar(255), 
	additional_value varchar(255), 
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	primary key (product_id, element_id)
);

alter table banking_product_deposit_rates add constraint FKo9gvl59ouc6nbgtbshmmvqmh9 foreign key (product_id) references banking_products;

create table banking_product_deposit_rate_tiers (
	product_id varchar(255) not null, 
	element_id integer not null, 
	element_id_2 integer not null, 
	additional_info varchar(255),
	additional_info_uri varchar(255),
	maximum_value double,
	minimum_value double not null,
	name varchar(255),
	rate_application_method varchar(255),
	unit_of_measure varchar(255),
	appl_cond_additional_info varchar(255),
	appl_cond_additional_info_uri varchar(255),	
	primary key (product_id, element_id, element_id_2)
);

alter table banking_product_deposit_rate_tiers add constraint FKnumucr9a4705u14gxe0nlnyih foreign key (product_id, element_id) references banking_product_deposit_rates;

create table banking_product_lending_rates (
	product_id varchar(255) not null, 
	element_id integer not null, 
	lending_rate_type varchar(255), 
	rate double not null, 
	comparison_rate double not null, 
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	additional_value varchar(255), 
	application_frequency varchar(255), 
	calculation_frequency varchar(255), 
	interest_payment_due varchar(255), 
	loan_purpose varchar(255), 
	repayment_type varchar(255), 
	primary key (product_id, element_id)
);

alter table banking_product_lending_rates add constraint FKjn2h2quo4av26udym1bxng32 foreign key (product_id) references banking_products;

create table banking_product_lending_rate_tiers (
	product_id varchar(255) not null, 
	element_id integer not null, 
	element_id_2 integer not null,
	additional_info varchar(255),
	additional_info_uri varchar(255),
	maximum_value double,
	minimum_value double not null,
	name varchar(255),
	rate_application_method varchar(255),
	unit_of_measure varchar(255),
	appl_cond_additional_info varchar(255),
	appl_cond_additional_info_uri varchar(255),			
	primary key (product_id, element_id, element_id_2)
);

alter table banking_product_lending_rate_tiers add constraint FKacy41qr00pd5vam1tb7f224os foreign key (product_id, element_id) references banking_product_lending_rates;

create table banking_product_fees (
	product_id varchar(255) not null, 
	element_id integer not null, 
	accrual_frequency varchar(255), 
	accrued_rate double, 
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	additional_value varchar(255), 
	amount double, 
	balance_rate double, 
	currency varchar(255), 
	fee_type varchar(255), 
	name varchar(255), 
	transaction_rate double, 
	primary key (product_id, element_id)
);

alter table banking_product_fees add constraint FKqesqmrqfjcx2v45v88jmqbr3s foreign key (product_id) references banking_products;

create table banking_product_discounts (
	product_id varchar(255) not null, 
	element_id integer not null, 
	element_id_2 integer not null, 
	accrued_rate double, 
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	additional_value varchar(255), 
	amount double, 
	balance_rate double, 
	description varchar(255), 
	discount_type varchar(255), 
	fee_rate double, 
	transaction_rate double, 
	primary key (product_id, element_id, element_id_2)
);

alter table banking_product_discounts add constraint FK6h0ofrnbk1w2ttd86vmrvdol0 foreign key (product_id, element_id) references banking_product_fees;

create table banking_product_discount_eligibilities (
	element_id integer not null, 
	element_id_2 integer not null, 
	element_id_3 integer not null, 
	product_id varchar(255) not null, 
	additional_info varchar(255), 
	additional_info_uri varchar(255), 
	additional_value varchar(255), 
	discount_eligibility_type varchar(255), 
	primary key (product_id, element_id, element_id_2, element_id_3)
);

alter table banking_product_discount_eligibilities add constraint FKfxjm4cvbp4iruhfvj8wv8pv94 foreign key (product_id, element_id, element_id_2) references banking_product_discounts;


