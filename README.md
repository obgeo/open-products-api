Open Product API Reference Implementation
---

## About This Repository
This repository contains the Reference Implementation (RI) for Georgian OpenFinance Open Product API

## How to Build and Run
### Using Docker Compose ###
Make sure docker and docker compose are installed on your computer. Navigate to the project root directory and run

    docker-compose up
During the first run, the application will be built from the sources and run together with the Posgtres DB.

### Swagger/OpenAPI Console ###
If there are no errors during docker-compose execution stage, start using the applicatoin by opening Swagger UI in the web browser.
Swagger UI is located at http://localhost:18080/swagger-ui.html

### Database Content
The project uses [Flyway (flywaydb.org)](https://flywaydb.org/) to create the database schema. Corresponding scripts are provided in src/man/resources/db/migration folder (for H2 and Postgres). There are 2 scripts per database:

 1. DB schema creation
 2. Sample data load
Feel free to customize the sample data load script. DB schema creation script customization is not recommended for inexperienced users. The application will try to validate database schema and fail if the schema objects are not configured properly.

### Development and Contribution
The application is written using [Spring Boot](https://spring.io/projects/spring-boot) and it's very easy to extend.
Any meaningful contributions will be highly appreciated.
 
 